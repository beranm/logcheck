# Log check role

In case you want to put new logs into the role, open `vars/main.yml` and insert them there.

App dependencies:

 - Perl
 - zabbix-agent (`zabbix_sender`)

One of the requisition is also a key on Zabbix server: `log.check` It's a Zabbix trapper, so create your `item` according to that.
